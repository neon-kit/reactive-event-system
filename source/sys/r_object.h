/*
 * r_object.h
 *
 *  Created on: Nov 6, 2022
 *      Author: nenad
 */

#ifndef SYS_R_OBJECT_H_
#define SYS_R_OBJECT_H_

#include <stdint.h>

#include "reactive_config.h"

typedef uint_fast8_t r_object__id;

struct r_object__equeue
{
	uint_fast8_t * queue_storage;
	uint_fast8_t head;
	uint_fast8_t tail;
	uint_fast8_t size;
	uint_fast8_t free;
};

struct r_object__smp
{
	void (*state)(void *, uint_fast8_t);
};

struct r_object__epa
{
	struct r_object__equeue eq;
	struct r_object__smp smp;
};

struct r_object__etimer
{
	r_object__id epa_id;
	r_object__id event_id;
};

struct r_object__etimer_service
{

};

struct r_object__epn
{

};

#endif /* SYS_R_OBJECT_H_ */
