/*
 * reactive_port.h
 *
 *  Created on: Nov 6, 2022
 *      Author: nenad
 */

#ifndef REACTIVE_PORT_H_
#define REACTIVE_PORT_H_

#include <stdint.h>

#if defined(__DOXYGEN__)
#define R_PORT__CRITICAL_ENTER()
#define R_PORT__CRITICAL_EXIT()
#endif

void r_port__critical_init(void);

#if defined(__DOXYGEN__)
#define R_PORT__SLEEP_WAIT()
#define R_PORT__SLEEP_WAKEUP()
#endif

void r_port__sleep_init(void);

void r_port__timer_init(void);
void r_port__timer_start(uint32_t timeout_ms);
void r_port__timer_stop(void);

#endif /* REACTIVE_PORT_H_ */
