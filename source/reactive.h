/**
 * @file
 * @date        Nov 3, 2022
 * @brief       Reactive Framework interface header
 *
 * Main API header file. This is usually the only needed header file for 
 * application to use the Reactive Framework. It does not include additional 
 * header files besides standard C files. All pointer types defined in this 
 * header are opaque types to objects. The application uses
 * `rf__app_config.h` functions to allocate objects.
 *
 * This header defines:
 * - Public API macros
 * - Public API integral types
 * - Public API opaque types
 * - Public API functions
 *
 * @author      Nenad Radulovic (nenad.b.radulovic@gmail.com)
 * @authors     Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @defgroup    rf_intf Interface
 * @brief       Reactive Framework (R) interface
 *
 * @{
 */
#ifndef REACTIVE_H_
#define	REACTIVE_H_

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>
    
/**
 * @defgroup    rf_event Events
 * @brief       Event definition
 * @{
 */
    
/**
 * @brief       Event identifier type
 * 
 * Reactive Framework support up to 256 different events.
 */
typedef uint_fast8_t r_event__id;

/**
 * @brief       Reserved event identifiers.
 * 
 * Reactive Framework reserves first 16 identifiers for use by state machine
 * operation. User application adds new identifiers starting from this value.
 */
#define R_EVENT__ID_USER        16

/** @} */
/**
 * @defgroup    rf_sm State machine
 * @brief       State machine definition
 * @{
 */

/**
 * @brief       State machine super event
 * 
 * This event is sent by state machine processor when it needs to know what is
 * the super state of the current state. The current state needs to respond to
 * this event by calling function @ref r_sm__super_state with the argument set
 * to super state function.
 */
#define R_SM__EVENT_SUPER       0

/**
 * @brief       State machine initialize event
 * 
 * This event is sent by state machine processor when it wants to signal to
 * state machine that we have transitioned to destination state. State machine
 * may use this event to either call:
 * - @ref r_sm__super_state - when the destination state does no handle 
 *   initialize event.
 * - @ref r_sm__event_handled - when the destination state does handle 
 *   initialize event.
 * - @ref r_sm__transit_to - when the destination state wants to execute 
 *   eventless transition to another state.
 */
#define R_SM__EVENT_INIT        1

/**
 * @brief       State machine enter event
 * 
 * This event is sent by state machine processor when it wants to notify state
 * machine that the state machine has transitioned to the state.
 */
#define R_SM__EVENT_ENTER       2

/**
 * @brief       State machine exit event
 * 
 * This event is sent by state machine processor when it wants to notify state
 * machine that the state machine has transitioned from the state.
 */
#define R_SM__EVENT_EXIT        3

/**
 * @brief       State machine NULL event
 * 
 * This event is received by state machine when a event was canceled. The state
 * should not handle this event in any particular way. This event is just a
 * shortcut for canceled events.
 */
#define R_SM__EVENT_NULL        4

/**
 * @brief       State machine state function prototype
 */
typedef void (r_sm__state)(r_event__id, void *);

/**
 * @brief       State machine super state
 * 
 * State machine calls this function when it wants to say to state machine
 * processor what is the super state of the current state.
 * 
 * If the current state is top state and it has no super state it is valid to
 * provide a NULL pointer argument.
 * 
 * @param       super_state Pointer to super state
 */
void r_sm__super_state(r_sm__state * super_state);

/**
 * @brief       State machine event is handled
 * 
 * State machine should call this function when an event is handled.
 */
void r_sm__event_handled(void);

/**
 * @brief       State machine wants to transit to another state.
 * 
 * @pre         Argument `new_state` must be non-NULL pointer.
 */
void r_sm__transit_to(r_sm__state * new_state);

/** @} */
/**
 * @defgroup    rf_agent Agent
 * @brief       Agent definition
 * @{
 */

/**
 * @brief       Agent attributes
 */
struct r_agent__attr
{
    r_event__id * event_queue;          //!< Event queue storage
    size_t event_queue_size;            //!< Size of event queue in bytes
    r_sm__state * initial_state;        //!< Initial state of state machine
};

/**
 * @brief       Agent identification type
 */
typedef uint_fast8_t r_agent__id;

/**
 * @brief       Create a new agent.
 * @param       attr Pointer to agent attributes structure.
 * @param       prio Priority of the agent. The higher is the number the higher
 *              is priority the agent.
 * @param       ws Pointer to agent workspace. This pointer is passed to state
 *              machine. It can point to any data structure which the state
 *              machine can access. If the state machine does not need this data
 *              structure, set this pointer to NULL.
 * @return      Agent identification
 * 
 * @pre         Argument `attr` must be non-NULL pointer.
 * @pre         Argument `prio` must be integer in range 0-255 inclusive. 
 * @pre         The priority value must be unique to the agent being created.
 * @pre         The priority value must be less then 
 *              @ref R_CONFIG__AGENT_NUMBER_OF_INSTANCES.
 */
r_agent__id r_agent__create(const struct r_agent__attr * attr, uint_fast8_t prio, void * ws);

/**
 * @brief       Send the event @a event_id to the agent @a agent_id.
 * @param       agent_id Previously created agent with @a agent_id 
 *              identification.
 * @param       event_id Event with @a event_id which is to be sent to the agent.
 */
void r_agent__send(r_agent__id agent_id, r_event__id event_id);
void r_agent__send_isr(r_agent__id agent_id, r_event__id event_id);
void r_agent__send_front(r_agent__id agent_id, r_event__id event_id);
void r_agent__send_front_isr(r_agent__id agent_id, r_event__id event_id);

/** @} */

typedef uint_fast8_t r_timer__id;

r_timer__id r_timer__create(void);
void r_timer__send_after(r_timer__id timer_id, r_event__id event_id, uint32_t after_ms);
void r_timer__send_every(r_timer__id timer_id, r_event__id event_id, uint32_t every_ms);
void r_timer__cancel(r_timer__id timer_id);

void r_sys__start(void);
void r_sys__timer_tick(void);

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* REACTIVE_H_ */
/** @} */
